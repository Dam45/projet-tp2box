import yaml

class Thing:
    def __init__(self, vol, name=None):
        self._volume=vol
        self._name=name
    
    def volume(self):
        return self._volume
     
    def set_name(self, name):
        self._name=name
    
    def __repr__(self):
        return "("+self._name+", "+ str(self._volume)+")"

    def repr(self):
        return self._name
    
    def has_name(self, name):
        return self._name==name

    @staticmethod            
    def from_yaml(data):
        name = data.get("name", None)
        volume = data.get("volume", 0)
        res = Thing(volume, name)
        return res
        
class Box:
    def __init__(self, is_open=True, capacity=None):
        self._contents=[]
        self._open = is_open
        self._capacity = capacity
        
    def add(self, elem):
        if self._open :
            if self.has_room_for(elem):
                self._contents.append(elem)
                if(self._capacity != None):
                    self._capacity = self._capacity - elem.volume()
            
        
    def __contains__(self, elem):
        return elem in self._contents
        
    def remove(self, elem):
        self._contents.remove(elem)
        
    def isOpen(self):
        return self._open
        
    def isClose(self):
        return not(self._open)
        
    def open(self):
        self._open=True
    
    def close(self):
        self._open=False
        
    def action_look(self):
        return ", ".join(self._contents)
     
    def capacity(self):
        return self._capacity

    def set_capacity(self, capa):
        self._capacity = capa
        
    def has_room_for(self, t):
        if self._capacity == None :
            return True
        elif self._capacity >= t.volume() :
            return True
        else :
            return False   
            
    def find(self, name):
        if self._open :
            found = False
            i=0
            while(not found and i < len(self._contents)):
                if self._contents[i].repr()==name:
                    found = True
                else:
                    i = i+1
            return found
        else:
            return None

    def __repr__(self):
        res = "box "
        if(self._open):
            res = res+ "open "
        else :
            res = res+ "close "
        res = res + " remaining capacity " + str(self._capacity) + " contents " + str(self._contents)
        return res
            
    @staticmethod            
    def from_yaml(data):
        is_open = data.get("is_open", True)
        capacity = data.get("capacity", None)
        contents = data.get("contents", [])
        res = Box(is_open, capacity)
        for thing in contents :
            t = Thing.from_yaml(thing)
            res.add(t)
        return res


    @staticmethod            
    def from_file_yaml(name):
        with open(name,'r') as flux:
            l = yaml.load(flux)
            #print(l)
            res=[]
            for b in l :
                bo = Box.from_yaml(b)
                res.append(bo)
            return res

    @staticmethod            
    def from_file_all_yaml(name):
        with open(name,'r') as flux:
            l = yaml.load_all(flux)
            res=[]
            for b in l :
                bo = Box.from_yaml(b)
                res.append(bo)
            return res



