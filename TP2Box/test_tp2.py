from tp2 import *

def test_creationBoite():
    b = Box()
    
def test_addInBox():
    b= Box()
    b.add("truc1");
    b.add("truc2");
    
def test_inBox():
    b= Box()
    b.add("truc1");
    b.add("truc2");
    assert ("truc1" in b) == True
    assert ("machin" in b) == False
    
def test_removeBox():
    b= Box()
    b.add("truc1");
    b.add("truc2");
    assert ("truc1" in b) == True
    b.remove("truc1");        
    assert ("truc1" in b) == False
   
def test_Open():
    b = Box()
    assert b.isOpen() == True
    assert b.isClose() == False
    b.close()
    assert b.isOpen() == False
    assert b.isClose() == True

def test_actionLook():
    b= Box()
    b.add("truc1");
    b.add("truc2");
    assert b.action_look() == "truc1, truc2"
    
def test_thing():
    chose = Thing(3)
    assert chose.volume()==3
    
def test_capacity():
    b=Box()
    assert b.capacity()==None
    b.set_capacity(5)
    assert b.capacity()==5
    
def test_has_room_for():
    b=Box()
    t=Thing(3)
    assert b.has_room_for(t)==True
    b.set_capacity(5)
    assert b.has_room_for(t)==True
    b.add(t)
    t=Thing(4)
    assert b.has_room_for(t)==False

  
def test_find():
    b= Box()
    t=Thing(3)
    t.set_name("truc1");
    b.add(t);
    t=Thing(4)
    t.set_name('truc2')
    b.add(t);
    assert b.find("truc1")==True
    assert b.find("bidule")==False 
    
